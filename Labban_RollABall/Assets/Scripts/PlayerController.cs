﻿using UnityEngine;

// Include the namespace required to use Unity UI and Input System
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

	// Create public variables for player speed, and for the Text UI game objects
	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;

	private float movementX;
	private float movementY;

	private Rigidbody rb;
	private int count;

    public float jumpPower;//How much force our jump has
    bool OnGround = false;//Whether the ball is currently resting on ground or not

	// At the start of the game..
	void Start()
	{
		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText();

		// Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
		winTextObject.SetActive(false);
	}

	private void Update()
    {
		if (transform.position.y < -0.5f)
        {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
		/*Shoots a raycast which checks in a line for a collision.
		 * We shoot the ray from the center of the player's position, directly downward, a distance of .51 Unity units (just barely below the ball)
		 * Returns true if it hits something and false if it doesn't.
		 */
    }

    void FixedUpdate()
	{
		// Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);
	}

	//You need to add a jump action and bindings for it to your InputActions asset settings for the following to get called:
	void OnJump()
    {
		Jump();
    }
	void Jump()
    {
		if (OnGround)//Only let us jump if we are on the ground
			rb.AddForce(Vector3.up * jumpPower);//Actually apply upward force to the ball to send it into the air
    }
	void OnTriggerEnter(Collider other)
	{
		// ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText();

			//Grow the ball each time we pick up a collectable:
			float growthAmount = .1f;
			transform.localScale += new Vector3(growthAmount, growthAmount, growthAmount);
		}
	}
	
    void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		movementX = v.x;
		movementY = v.y;
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();

		if (count >= 12)
		{
			// Set the text value of your 'winText'
			winTextObject.SetActive(true);
		}
	}
}